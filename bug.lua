
local base_image = "foo.png"
local black_image = base_image.."^[noalpha^[multiply:#000000^[resize:2x1"
local white_image = base_image.."^[noalpha^[multiply:#000000^[invert:rgb^[resize:2x1"
local clear_image = white_image.."^[makealpha:255,255,255"

local tiled_image_2x2 = "[combine:2x2"..
	":0,0="..(clear_image:gsub("%^", "\\^"):gsub(":", "\\:"))..
	":0,1="..(white_image:gsub("%^", "\\^"):gsub(":", "\\:"))
local tiled_image_4x4 = "[combine:4x4"..
	":0,0="..(clear_image:gsub("%^", "\\^"):gsub(":", "\\:"))..
	":2,0="..(white_image:gsub("%^", "\\^"):gsub(":", "\\:"))..
	":0,1="..(white_image:gsub("%^", "\\^"):gsub(":", "\\:"))..
	":2,1="..(clear_image:gsub("%^", "\\^"):gsub(":", "\\:"))..
	":0,2="..(clear_image:gsub("%^", "\\^"):gsub(":", "\\:"))..
	":2,2="..(white_image:gsub("%^", "\\^"):gsub(":", "\\:"))..
	":0,3="..(white_image:gsub("%^", "\\^"):gsub(":", "\\:"))..
	":2,3="..(clear_image:gsub("%^", "\\^"):gsub(":", "\\:"))..
	""

minetest.register_node(":bug:okay", {
	description = "Should be black and white.  Compare the wielded node to the placed node.",
	use_texture_alpha = "blend",
	tiles = { black_image, },
	overlay_tiles = { "", "", tiled_image_2x2, },	-- Sides only.
})

minetest.register_node(":bug:broken", {
	description = "Should also be black and white.  Compare the wielded node to the placed node.",
	use_texture_alpha = "blend",
	tiles = { { name = black_image, align_style="world", scale=2, }, },
	overlay_tiles = { "", "", { name = tiled_image_4x4, align_style="world", scale=2, }, },	-- Sides only.
})

